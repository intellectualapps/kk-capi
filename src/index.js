const express = require('express');
const cors = require('cors');
const path = require('path');
const app = express();

const port = process.env.PORT || 5000;
app.use(cors());
app.use(express.json());

app.get('/', (req, res) => {
  res.type('html').sendFile(path.join(`${__dirname}/public/index.html`));
});

// Handle undefined routes
app.use('*', (_req, res) => {
  res.status(404).json({
    success: false,
    message: 'Resource not available'
  });
});

app.listen(port, () => {
  console.log(`API running on port ${port}`);
});